# tfom23-demo

The used setup to demonstrate FabAccess project at "the future of making" (tfom) 2023 (Interfacer / Fab City). Details can be found [here](https://docs.fab-access.org/books/blog-historie/page/03032023-interfacer-abschlussveranstaltung-zur-konferenz-und-expo-the-future-of-making-tfom-2023).