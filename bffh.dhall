{- Main configuration file for bffh
 - ================================
 -
 - In this configuration file you configure almost all parts of how bffh operates, but most importantly:
 -      * Machines
 -      * Initiators and Actors
 -      * Which Initiators and Actors relate to which machine(s)
 -      * Roles and the permissions granted by them
 -}

-- The config is in the configuration format/language dhall. You can find more information about dhall over at
-- https://dhall-lang.org

-- (Our) Dhall is somewhat similar to JSON and YAML in that it expects a top-level object containing the
-- configuration values
{
    -- Configure the addresses and ports bffh listens on
    listens = [
        -- BFFH binds a port for every listen object in this array.
        -- Each listen object is of the format { address = <STRING>, port = <INTEGER> }
        -- If you don't specify a port bffh will use the default of `59661`
        -- 'address' can be a IP address or a hostname
        -- If bffh can not bind a port for the specified combination if will log an error but *continue with the remaining ports*
        { address = "::", port = Some 59661 }
    ],

    -- Configure TLS. BFFH requires a PEM-encoded certificate and the associated key as two separate files
    certfile = "tfom23-demo/certs/self-signed-cert.pem",
    keyfile = "tfom23-demo/certs/self-signed-key.pem",

    -- BFFH right now requires a running MQTT broker.
    mqtt_url = "mqtt://localhost:1883",

    -- Path to the database file for bffh. bffh will in fact create two files; ${db_path} and ${db_path}.lock.
    -- BFFH will *not* create any directories so ensure that the directory exists and the user running bffh has write
    -- access into them.
    db_path = "/var/lib/bffh/db",

    -- Audit log path. Bffh will log state changes into this file, one per line.
    -- Audit log entries are for now JSON:
    -- {"timestamp":1641497361,"machine":"Testmachine","state":{"state":{"InUse":{"uid":"Testuser","subuid":null,"realm":null}}}}
    auditlog_path = "/tmp/bffh.audit",

    -- set logging to verbose
    verbosity = 3,
    spacename = Some "TFOM23-Demo",
    instanceurl = Some "tfom23-demo.fab-access.org",

    -- In dhall you can also easily import definitions from other files, e.g. you could write
    -- roles = ./roles.dhall
    roles = 
    {
        -- Role definitions
        -- A role definition is of the form
        -- rolename = {
        --    parents = [<list of role names to inherit from>],
        --    permissions = [<list of perm rules>],
        -- }
        --
        -- Role names are case sensitive, so RoleName != rolename.
        --
        -- If you want either parents or permissions to be empty its best to completely skip it:
        
        Default = {
            permissions = 
            [
                "tfom23.disclose",
                "tfom23.read",
            ]
        },
        Admin = 
        { 
            permissions = 
            [
                "tfom23.*",
                "bffh.users.info",
                "bffh.users.manage",
                "bffh.users.admin",
            ]
        },
        Manage = 
        {
            permissions = 
            [ 
                "tfom23.manage",
            ]
        },

        EuroBox = 
        {
            permissions = 
            [ 
                "tfom23.eurobox.write",
            ]
        },

        LBoxx = 
        {
            permissions = 
            [ 
                "tfom23.lboxx.write",
            ]
        },
        Locker = 
        {
            permissions = 
            [ 
                "tfom23.locker.write",
            ]
        },
        Lasercutter = 
        {
            permissions = 
            [ 
                "tfom23.lasercutter.write",
            ]
        },
        Printer = 
        {
            permissions = 
            [ 
                "tfom23.printer.write",
            ]
        },
        Prusa = 
        {
            permissions = 
            [ 
                "tfom23.prusa.write",
            ]
        },
        CNC = 
        {
            permissions = 
            [ 
                "tfom23.cnc.write",
            ]
        },
    },

    -- Configure machines
    -- "Machines" (which in future will be more appropiately named "resources") are the main thing bffh is concerned
    -- with.
    -- You can define an almost limitless amount of machines (well 2^64 - 1, so 18_446_744_073_709_551_615 to be precise)
    -- Each of these machines can then have several "actors" and "initiators" assigned
    machines = {
        -- FabLock with LBoxx
        LBoxx_0 = 
        { 
            name = "Filament",
            description = Some "LBoxx with 1,75mm PLA",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.lboxx.write",
            manage = "tfom23.manage",
            category = "LBoxx",
            prodable = True,
        },
        LBoxx_1 = 
        { 
            name = "FabLock Tools",
            description = Some "LBoxx with Tools of the FabLock Project",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.lboxx.write",
            manage = "tfom23.manage",
            category = "LBoxx",
            prodable = True,
        },
        LBoxx_2 = 
        { 
            name = "FabReader Tools",
            description = Some "LBoxx with Tools of the FabReader Project",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.lboxx.write",
            manage = "tfom23.manage",
            category = "LBoxx",
            prodable = True,
        },
        LBoxx_3 = 
        { 
            name = "Sticker",
            description = Some "LBoxx with FabAccess Sticker and NTAGs",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.lboxx.write",
            manage = "tfom23.manage",
            category = "LBoxx",
            prodable = True,
        },
        LBoxx_4 = 
        { 
            name = "Demo Parts",
            description = Some "LBoxx with Parts for the TFOM23 Demo",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.lboxx.write",
            manage = "tfom23.manage",
            category = "LBoxx",
            prodable = True,
        },

        -- FabLock with EuroBox
        EuroBox_0 = 
        { 
            name = "Haribo",
            description = Some "EuroBox with Haribo",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.eurobox.write",
            manage = "tfom23.manage",
            category = "EuroBox",
            prodable = True,
        },
        EuroBox_1 = 
        { 
            name = "Goldschatz",
            description = Some "EuroBox with Rittersport Goldschatz",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.eurobox.write",
            manage = "tfom23.manage",
            category = "EuroBox",
            prodable = True,
        },

        -- FabLock with Kallax
        Drawer_0 = 
        { 
            name = "Drawer 0 ???",
            description = Some "Kallax Drawer",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.locker.write",
            manage = "tfom23.manage",
            category = "Locker",
            prodable = True,
        },
        Drawer_1 = 
        { 
            name = "Drawer 1 ???",
            description = Some "Kallax Drawer",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.locker.write",
            manage = "tfom23.manage",
            category = "Locker",
            prodable = True,
        },
        Drawer_2 = 
        { 
            name = "Drawer 2 ???",
            description = Some "Kallax Drawer",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.locker.write",
            manage = "tfom23.manage",
            category = "Locker",
            prodable = True,
        },
        Drawer_3 = 
        { 
            name = "Drawer 3 ???",
            description = Some "Kallax Drawer",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.locker.write",
            manage = "tfom23.manage",
            category = "Locker",
            prodable = True,
        },
        Door_0 = 
        { 
            name = "3D-Printer Accesories",
            description = Some "Kallax Door",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.locker.write",
            manage = "tfom23.manage",
            category = "Locker",
            prodable = True,
        },

        -- Machines
        Printer_0 = 
        { 
            name = "Prusa MK3",
            description = Some "FabAccess Prusa MK3",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.prusa.write",
            manage = "tfom23.manage",
            category = "Printers"
        },
        Printer_1 = 
        { 
            name = "Other Printer ???",
            description = Some "TFOM23 Printer",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.printer.write",
            manage = "tfom23.manage",
            category = "Printers"
        },
        Lasercutter_0 = 
        { 
            name = "Other Lasercutter ???",
            description = Some "TFOM23 Lasercutter",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.lasercutter.write",
            manage = "tfom23.manage",
            category = "Lasercutter"
        },
        CNC_0 = 
        { 
            name = "Some Open Hardware CNC Router",
            description = Some "TFOM23 CNC",
            disclose = "tfom23.disclose",
            read = "tfom23.read",
            write = "tfom23.cnc.write",
            manage = "tfom23.manage",
            category = "CNC"
        },
    },

    -- Actor configuration. Actors are how bffh affects change in the real world by e.g. switching a power socket
    -- using a shelly
    actors = {
        -- Actors similarly to machines have an 'id'. This id (here "Shelly1234") is limited to Alphanumeric ASCII
        -- and must begin with a letter.
        shellyplug-s-C8C9A3B942DB = { module = "Shelly", params = {=} },
        shellyplug-s-C8C9A3B943D7 = { module = "Shelly", params = {=} },
        shellyplug-s-C8C9A3B8DB67 = { module = "Shelly", params = {=} },
        shellyplug-s-3CE90ED72CEF = { module = "Shelly", params = {=} },
        shellyplug-s-3CE90ED72481 = { module = "Shelly", params = {=} },
        shellyplug-s-C8C9A3B8E88A = { module = "Shelly", params = {=} },
        shellyplug-2C94AA = { module = "Shelly", params = {=} },
        shellyplug-C198E8 = { module = "Shelly", params = {=} },

        -- Kallax Locker
        fablock_locker_0 = { module = "Process", params = 
        { 
            cmd = "python",
            args = "tfom23-demo/actors/fablock/main.py --host localhost --fablock 00000 --lock 00000"
        }},
        fablock_locker_1 = { module = "Process", params = 
        { 
            cmd = "python",
            args = "tfom23-demo/actors/fablock/main.py --host localhost --fablock 00000 --lock 00001"
        }},
        fablock_locker_2 = { module = "Process", params = 
        { 
            cmd = "python",
            args = "tfom23-demo/actors/fablock/main.py --host localhost --fablock 00000 --lock 00002"
        }},
        fablock_locker_3 = { module = "Process", params = 
        { 
            cmd = "python",
            args = "tfom23-demo/actors/fablock/main.py --host localhost --fablock 00000 --lock 00003"
        }},
        fablock_locker_4 = { module = "Process", params = 
        { 
            cmd = "python",
            args = "tfom23-demo/actors/fablock/main.py --host localhost --fablock 00000 --lock 00004"
        }},
        fablock_locker_5 = { module = "Process", params = 
        { 
            cmd = "python",
            args = "tfom23-demo/actors/fablock/main.py --host localhost --fablock 00000 --lock 00005"
        }},
        fablock_locker_6 = { module = "Process", params = 
        { 
            cmd = "python",
            args = "tfom23-demo/actors/fablock/main.py --host localhost --fablock 00000 --lock 00006"
        }},

        -- LBoxx Rack
        fablock_lboxx_0 = { module = "Process", params = 
        { 
            cmd = "python",
            args = "tfom23-demo/actors/fablock/main.py --host localhost --fablock 00001 --lock 00000"
        }},
        fablock_lboxx_1 = { module = "Process", params = 
        { 
            cmd = "python",
            args = "tfom23-demo/actors/fablock/main.py --host localhost --fablock 00001 --lock 00001"
        }},
        fablock_lboxx_2 = { module = "Process", params = 
        { 
            cmd = "python",
            args = "tfom23-demo/actors/fablock/main.py --host localhost --fablock 00001 --lock 00002"
        }},
        fablock_lboxx_3 = { module = "Process", params = 
        { 
            cmd = "python",
            args = "tfom23-demo/actors/fablock/main.py --host localhost --fablock 00001 --lock 00003"
        }},
        fablock_lboxx_4 = { module = "Process", params = 
        { 
            cmd = "python",
            args = "tfom23-demo/actors/fablock/main.py --host localhost --fablock 00001 --lock 00004"
        }},

        -- FabReader
        fabreader_0 = { module = "Process", params = 
        {
            cmd = "python",
            args = "tfom23-demo/actors/fabreader/main.py --host localhost --fabreader 00000"
        }},
        fabreader_1 = { module = "Process", params = 
        {
            cmd = "python",
            args = "tfom23-demo/actors/fabreader/main.py --host localhost --fabreader 00001"
        }},
        fabreader_2 = { module = "Process", params = 
        {
            cmd = "python",
            args = "tfom23-demo/actors/fabreader/main.py --host localhost --fabreader 00002"
        }},
        fabreader_3 = { module = "Process", params = 
        {
            cmd = "python",
            args = "tfom23-demo/actors/fabreader/main.py --host localhost --fabreader 00003"
        }},
        fabreader_4 = { module = "Process", params = 
        {
            cmd = "python",
            args = "tfom23-demo/actors/fabreader/main.py --host localhost --fabreader 00004"
        }},

        -- Fabpel
        fabpel_0 = { module = "Process", params = 
        {
            cmd = "python",
            args = "tfom23-demo/actors/fabpel/main.py --host localhost --fabpel 00000"
        }},
        fabpel_1 = { module = "Process", params = 
        {
            cmd = "python",
            args = "tfom23-demo/actors/fabpel/main.py --host localhost --fabpel 00001"
        }},
        fabpel_2 = { module = "Process", params = 
        {
            cmd = "python",
            args = "tfom23-demo/actors/fabpel/main.py --host localhost --fabpel 00002"
        }},
        fabpel_3 = { module = "Process", params = 
        {
            cmd = "python",
            args = "tfom23-demo/actors/fabpel/main.py --host localhost --fabpel 00003"
        }},
    },

    -- Linkng up machines to actors
    -- Actors need to be connected to machines to be useful. A machine can be connected to multiple actors, but one
    -- actor can only be connected to one machine.
    actor_connections = [
        { machine = "LBoxx_0", actor = "fablock_lboxx_0" },
        { machine = "LBoxx_1", actor = "fablock_lboxx_1" },
        { machine = "LBoxx_2", actor = "fablock_lboxx_2" },
        { machine = "LBoxx_3", actor = "fablock_lboxx_3" },
        { machine = "LBoxx_4", actor = "fablock_lboxx_4" },

        { machine = "EuroBox_0", actor = "fablock_locker_0" },
        { machine = "EuroBox_1", actor = "fablock_locker_1" },
        { machine = "Drawer_0", actor = "fablock_locker_2" },
        { machine = "Drawer_1", actor = "fablock_locker_3" },
        { machine = "Drawer_2", actor = "fablock_locker_4" },
        { machine = "Drawer_3", actor = "fablock_locker_5" },
        { machine = "Door_0", actor = "fablock_locker_6" },

        { machine = "Printer_0", actor = "shellyplug-s-C8C9A3B942DB" },
        { machine = "Printer_1", actor = "shellyplug-s-3CE90ED72481" },
        { machine = "Lasercutter_0", actor = "shellyplug-s-C8C9A3B943D7" },
        { machine = "CNC_0", actor = "shellyplug-s-C8C9A3B8E88A" },

        { machine = "Printer_0", actor = "fabreader_1" },
        { machine = "Printer_1", actor = "fabreader_2" },
        { machine = "Lasercutter_0", actor = "fabreader_3" },
        { machine = "CNC_0", actor = "fabreader_4" },

        { machine = "Printer_0", actor = "fabpel_0" },
        { machine = "Printer_1", actor = "fabpel_1" },
        { machine = "Lasercutter_0", actor = "fabpel_2" },
        { machine = "CNC_0", actor = "fabpel_3" },
    ],

    -- Initiators are configured almost the same way as Actors, refer to actor documentation for more details
    -- The below '{=}' is what you need if you want to define *no* initiators at all and only use the API with apps
    -- to let people use machines.
    initiators =
    {
        =
    },
    -- The "Dummy" initiator will try to use and return a machine as the given user every few seconds. It's good to
    -- test your system but will spam your log so is disabled by default.
    --initiators = { Initiator = { module = "Dummy", params = { uid = "Testuser" } } },

    -- Linking up machines to initiators. Similar to actors a machine can have several initiators assigned but an
    -- initiator can only be assigned to one machine.
    -- The below is once again how you have to define *no* initiators.
    init_connections =
    [
        
    ] : List { machine : Text, initiator : Text }
}
